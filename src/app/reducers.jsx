import {combineReducers} from 'redux'
import * as actions from './actions'
import _assign from 'lodash/assign'

function showLoading(state = [], action) {
    switch (action.type) {
        case actions.SET_Loading_State:
            return action.show || false;
        default:
            return state;
    }
}

function setTitle(state = [], action) {
    switch (action.type) {
        case actions.Title:
            return action.title || {};
        default:
            return state;
    }
}

const defaultState = {
    setAddress: {}
}

function setAddress(state = defaultState, action) {
    switch (action.type) {
        case 'ADDRESS_GETLIST':
            return _assign({}, state, {ADDRESS_GETLIST: action.list});
        case 'ADDRESS_INFO':
            return _assign({}, state, {ADDRESS_INFO: action.list});
            // case 'ADDRESS_REMOVE':
            //     return state;
        default:
            return state;
    }
}

function setMyTip(state = [], action) {
    switch (action.type) {
        case actions.Show_Tip:
            return action.msg || {};
        default:
            return state;
    }
}

function operateAddr(state = [], action) {
    switch (action.type) {
        case actions.Operate_Address:
            return action.msg || {};
        default:
            return state;
    }
}

function minHeight(state = [], action) {
    switch (action.type) {
        case actions.Min_Height:
            return action.height || '';
        default:
            return state;
    }
}

const todoApp = combineReducers({showLoading, setTitle, setAddress, setMyTip, minHeight})

export default todoApp
