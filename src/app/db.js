import DBF from './dbFactory'

export default DBF.context;

DBF.create('Address', {
    getList : {
        url       : '/xplan/address/list',
    },
    remove: {
        url       : '/xplan/address/remove',
        type      : 'POST',
    },
    add: {
        url       : '/xplan/address/add',
        type      : 'POST',
    },
    info: {
        url       : '/xplan/address/info',
    },
    modify:{
        url       : '/xplan/address/modify',
        type      : 'POST',
    },
});