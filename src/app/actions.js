export const SET_Loading_State = 'SET_Loading_State'
export const Title = 'Title'
export const Address_GetList = 'Address_GetList'
export const Show_Tip = 'Show_Tip'
export const Min_Height = 'Min_Height'

export const ERRORMSG = {
    ERRMSG:'ERRMSG',
    BREAK:'BREAK',
}

export const ADDRESS = {
    GETLIST: 'ADDRESS_GETLIST',
    INFO:'ADDRESS_INFO',
    REMOVE:'ADDRESS_REMOVE',
}

export function setLoadingState(show) {
    return {
        type: SET_Loading_State,
        show
    }
}

export function setTitle(title) {
    return {
        type: Title,
        title
    }
}

// export function Address_getList(method,list) {
export function Address(method, list) {
	const mtd = method.toUpperCase();
    return {
        type:ADDRESS[mtd],
        list
    }
}

export function MyTip(msg) {
    return {
        type:Show_Tip,
        msg
    }
}

export function MinHeight(height) {
    return{
        type:Min_Height,
        height
    }
}
