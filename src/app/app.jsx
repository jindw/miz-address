'use strict';
//基本组件
import React, {Component} from 'react'
import {render} from 'react-dom';
import {attach} from 'fastclick';
import {Router, Route, hashHistory, IndexRoute} from 'react-router';

//自制组件
import Header from '../components/Header';
import Cookies from '../components/Cookie';

//页面
import Change from '../pages/Change';
import SelectAddress from '../pages/SelectAddress';
import Operate from '../pages/Operate';
import Loading from '../components/Loading';

import './whole.scss';

import todoApp from './reducers.jsx';
import {Provider, connect} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';

const store = createStore(todoApp);

import * as actions from './actions';
import _includes from 'lodash/includes';

attach(document.body);

class App extends Component {

    constructor(props) {
        super(props);
        Cookies.setCookie('userKey', localStorage.auth, 1);
        this.from = this.props.location.query.from;
        window.__store__ = store;
    }

    componentDidMount() {
        store.dispatch(actions.MinHeight($(window).height() - $('header').height()));
    }

    render() {
        const myTip = this.props.myTip;
        let errMsg = [];
        if (myTip.errorMsg) {
            errMsg = <section id={myTip.errorMsg
                ? 'showMyTip'
                : ''}>
                <div className="weui-mask"/>
                <div className="weui-dialog">
                    <div className="weui-dialog__bd">{myTip.errorMsg}</div>
                    <div className="weui-dialog__ft">
                        <a onClick={() => {
                            if (_includes([
                                -1, '21000'
                            ], myTip.status))
                                history.back(-1);
                            window.__store__.dispatch(actions.MyTip({errorMsg: ''}));
                        }} href="javascript:;" className="weui-dialog__btn weui-dialog__btn_primary">知道了</a>
                    </div>
                </div>
            </section>
        }

        return <section>
            <Header show={this.from}/>
            <div style={{
                marginTop: (this.from
                    ? 0
                    : '')
            }} id={this.from
                ? 'notitle'
                : ''} className='body'>
                {this.props.children}
            </div>
            {errMsg}
            <Loading show={this.props.showLoading}/>
        </section>;
    }
}

const appElement = document.getElementById('app');

function select(state) {
    return {myTip: state.setMyTip, showLoading: state.showLoading}
}

render((
    <Provider store={store}>
        <Router history={hashHistory}>
            <Route path="/" component={connect(select)(App)}>
                <IndexRoute component={SelectAddress}/>
                <Route path="change" component={Change}/>
                <Route path="select" component={SelectAddress}/>
                <Route path="operate" component={Operate}/>
                <Route path="*" component={SelectAddress}/>
            </Route>
        </Router>
    </Provider>
), appElement);
