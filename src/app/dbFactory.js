import Cookies from '../components/Cookie'
import _assign from 'lodash/assign'
import _map from 'lodash/map'
import _includes from 'lodash/includes'
import {connect} from 'react-redux'
import * as actions from './actions'

// const proxy = new Proxy({}, {
//     set: function(target, key, value, receiver) {
//         console.log(`setting ${key}!`);
//         return Reflect.set(target, key, value, receiver);
//     },
//     get: function(target, key, receiver) {
//         console.log(`getting ${key}!`);
//         return Reflect.get(target, key, receiver);
//     }
// })

export default {
    // __ : {},
    //
    // set(key, value) {
    //     // this.__[key] = value;
    //     // proxy[key] = value;
    // },
    // get(key) {
    //     // return this.__[key];
    //     // return proxy[key];
    // },
    create(name, methods) {
        this.context[name] = new DB(name, methods);
    },
    context : {
        link: data => this.context.Data = data,
        Data: {}
    }
}

let urlPrefix;
if (__LOCAL__) {
    urlPrefix = '//121.43.148.191:8308';
} else {
    urlPrefix = '//api.mizlicai.com';
}

class DB {
    constructor(name, methods) {
        _map(methods, (config, method) => this[method] = query => new request(config, query, name, method));
    }
}
class request {
    constructor(config, querys, name, method) {
        const mergeQuery = {
            os: 'h5',
            userKey: Cookies.getCookie('userKey')
        };

        return new Promise((resolve, reject) => {
            $.ajax({
                url: `${urlPrefix}${config.url}.json`,
                type: config.type,
                data: _assign(mergeQuery, querys),
                timeout: 15000,
                cache: false,
                beforeSend() {
                    setTimeout(() => __store__.dispatch(actions.setLoadingState(true)), 0)
                }
            }).done(resp => {
                if (resp.status === 'SUCCESS') {
                    if (_includes([
                        'add', 'modify'
                    ], method)) {
                        __store__.dispatch(actions.MyTip({errorMsg: '操作成功', status: -1}));
                    } else {
                        __store__.dispatch(actions.Address(method, resp));
                    }
                    return resolve(resp);
                } else {
                    __store__.dispatch(actions.MyTip(resp));
                }
            }).fail(() => {
                __store__.dispatch(actions.MyTip({errorMsg: '很遗憾,请求失败,请稍后再试！', status: -1}));
            }).always(() => setTimeout(() => __store__.dispatch(actions.setLoadingState(false)), 0))
        })
    }
}
