import React,{Component} from 'react';
import DB from '../../app/db'

export const Message = ComponsedComponent => class extends Component {

    constructor(props) {
        super(props); 
        this.state = {
            address:[],
            pager:{
                totalCount:0,
            },
        }
        
        this.addressId = this.props.location.query.id;
        if(this.addressId){
            DB.Address.info({addressId:this.addressId});
        }else{
            DB.Address.getList({
                pageNumber:1,
                pageSize:5,
            });
        }
    }

    render() {
        return <ComponsedComponent {...this.props} {...this.state} />;
    }
};