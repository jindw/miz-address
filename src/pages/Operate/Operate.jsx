import React,{Component} from 'react';
import {Message} from "./Message.jsx";
import autosize from 'autosize'
import DeleteModal from 'react-modal'
import ModalSelectPlace from '../../components/ModalSelectPlace'
import DB from '../../app/db'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'
import _assign from 'lodash/assign'
import './Operate.scss'
import { connect } from 'react-redux'
import * as actions from '../../app/actions'

class Operate extends Component {

	constructor(props) {
		super(props);
		this.state = {
			minHeight:0,
            deleteModalIsOpen:false,
            modalPlaceIsOpen:false,
            cityList:{
                sub:[],
            },
            countyList:[],
            placeOpen:false,
            err:'',
            errClass:'none',
            errTop:$(window).height() * 0.3,
            id:this.props.location.query.id,
		}
        this.countyList = {};
        this.hideErr = null;
        setTimeout(()=>this.props.dispatch(actions.setLoadingState(true)),0);
	}

	componentDidMount() {
        this.props.dispatch(actions.setTitle({
            title:'编辑地址',
            backBtn:true,
        }));
		
        this.first = true;
        this.switcheryDone = false;
	}

    componentDidUpdate() {
        if(this.refs.place&&this.first){
            autosize(this.refs.place);
            this.switchery = new Switchery(this.refs.checkbox);
            this.first = false;
            const address = this.props.address;
            this.setState({
                province: address.province,
                city:address.city,
                count:address.county,
                addMessageModalIsOpen:this.props.location.query.id&&!address.province,
            });
        }
    }

    openDeleteModal(){
        this.setState({deleteModalIsOpen: true});
    }

    closeDeleteModal(){
        this.setState({deleteModalIsOpen: false});
    }

    removeNow(){
        this.setState({
            deleteModalIsOpen: false,
        });
        DB.Address.remove({
            addressId:this.props.location.query.id,
            defaultAddress:this.refs.checkbox.checked,
        }).then(()=>history.back(-1));
    }

    _submit(){
        const addr = this.props.info.address||{};
        const [province,city,county,name,phone,address] = 
        [
            this.state.province||addr.province,
            this.state.city||addr.city,
            this.state.count||addr.count||'',
            this.refs.name.value,
            this.refs.tel.value,
            this.refs.place.value,
        ]

        let err;
        if(!name){
            err = '请输入收货人姓名';
        }else if(!phone){
            err = '请输入手机号码';
        }else if(!province){
            err = '请选择所在地区';
        }else if(address.length < 5){
            err = '请填写详细地址,至少5个字';
        }else if(!phone){
            err = '请输入正确的手机号码';
        }

        const message = {
            province,
            city,
            county,
            name,
            phone,
            address,
            defaultAddress:this.refs.checkbox.checked,
        }

        if(err){
            if(this.state.errClass !== 'none')return;
            this.setState({
                err,
                errClass:'zoomIn',
            });

            this.hideErr = setTimeout(()=>{
                this.setState({
                    errClass:'zoomOut',
                });
                this.hideErr = setTimeout(()=>{
                    this.setState({
                        errClass:'none',
                    });
                },800);
            },2000);
        }else if(!this.props.location.query.id){//add
            DB.Address.add(message);
        }else{//update
            DB.Address.modify(_assign(message,{id:this.props.location.query.id}))
        }
    }

    componentWillUnmount() {
        clearTimeout(this.hideErr);
        this.props.dispatch(actions.Address('info',{}));
    }

    changeTel(e){
        e.target.value = e.target.value.replace(/[^0-9]/g, "");
    }

    render() {
        const {minHeight,pagers,info} = this.props;
        const address = info.address||{};
        if((this.state.id&&!address.id)||(!this.state.id&&typeof(this.props.pager.totalCount)!=='number')){
            return null;
        }
        const moren = [];
        if(this.state.id&&address.defaultAddress||(!this.state.id&&!pagers.totalCount)){
            moren.push(<input type="checkbox" key='check' ref='checkbox' defaultChecked disabled/>);
            sessionStorage.firstOperate = true;
        }else{
            moren.push(<input type="checkbox" key='check' ref='checkbox'/>);
        }

        return <section id='operate' style={{minHeight}}>
        	<dl>
        		<dd>
        			<label>收货人姓名：</label>
        			<input defaultValue = {address.name} ref='name' type="text" placeholder='请输入收货人姓名'/>
        		</dd>
        		<dd>
        			<label>手机号码：</label>
        			<input onChange={this.changeTel} defaultValue = {address.phone} ref='tel' type="tel" placeholder='请输入手机号码'/>
        		</dd>
                <dd className='rightSvg' onClick={()=>this.setState({placeOpen:true})}>
                    <label>所在地区：</label>
                    <span>{this.state.province||address.province} {this.state.city||address.city} {this.state.count||address.count}</span>
                    <Arrow fill='#c7c7cc'/>
                </dd>
        		<dd>
        			<textarea defaultValue = {address.address} ref='place' 
                    placeholder='请填写详细地址,至少5个字'></textarea>
        		</dd>
        	</dl>
        	<section id="moren">
        		设置为默认地址
                {moren}
        	</section>

            <section style={{display:(this.props.location.query.id?'':'none')}} id="delete" onClick={this.openDeleteModal.bind(this)}>
                删除收货地址
            </section>
            <a href="javascript:;" onClick={this._submit.bind(this)}>保存</a>
            <span>（邮寄仅限中国大陆地区）</span>
            <section id='err' style={{top:this.state.errTop}} className={`animated ${this.state.errClass}`}>
                <a href="javascript:;">{this.state.err}</a>
            </section>
            <DeleteModal
                isOpen={this.state.deleteModalIsOpen}
                onAfterOpen={this.openDeleteModal.bind(this)}
                onRequestClose={this.closeDeleteModal.bind(this)}
                closeTimeoutMS={150}
            >
                <section className='delete'>
                    <p className='center'>确认要删除吗？</p>
                    <dl>
                        <dd onClick={this.closeDeleteModal.bind(this)}>取消</dd>
                        <dd onClick={this.removeNow.bind(this)}>确定</dd>
                    </dl>
                </section>
            </DeleteModal>
            <ModalSelectPlace
                open={this.state.placeOpen} 
                success={(province,city,count)=>{this.setState({province, city, count})}}
                closePlace={()=>{this.setState({placeOpen: false})}} 
            />
        </section>
    }
}

function select(state) {
    const addr = state.setAddress;
    const info = addr&&addr.ADDRESS_INFO||{};
    const pagers = addr.ADDRESS_GETLIST&&addr.ADDRESS_GETLIST.pager||{};
    return {
        info,
        pagers,
        minHeight:state.minHeight,
    }
}
export default connect(select)(Message(Operate))
