import React, {Component} from 'react';
import {Message} from "./Message.jsx";
import AddMessageModal from 'react-modal'
import './SelectAddress.scss'
import {connect} from 'react-redux'
import * as actions from '../../app/actions'
import DeleteModal from 'react-modal'
import DB from '../../app/db'

import Swipeout from 'rc-swipeout'
import '../../../node_modules/rc-swipeout/assets/index.css'

class SelectAddress extends Component {
    constructor(props) {
        super(props);
        this.state = {
            minHeight: 0,
            addMessageModalIsOpen: false
        }
    }

    componentDidMount() {
        this.props.dispatch(actions.setTitle({
            title: '地址选择',
            backBtn: true,
            rightBtn: ['管理', '/change']
        }));
    }

    toXplan(itm) {
        if (!itm.province) {
            this.setState({addMessageModalIsOpen: true, placeId: itm.id});
        } else if (sessionStorage.back) {
            location.href = sessionStorage.back;
            sessionStorage.addressId = itm.id;
        }
    }

    openAddMessageModal() {
        this.setState({addMessageModalIsOpen: true});
    }

    closeAddMessageModal() {
        this.setState({addMessageModalIsOpen: false});
    }

    openDeleteModal() {
        this.setState({deleteModalIsOpen: true});
    }

    closeDeleteModal() {
        this.setState({deleteModalIsOpen: false});
    }

    removeNow() {
        this.setState({deleteModalIsOpen: false});
        DB.Address.remove({
            // addressId:this.props.location.query.id,
            addressId: this.state.addressId,
            // defaultAddress:this.refs.checkbox.checked,
            defaultAddress: this.state.defaultAddress
        }).then(() => {
            DB.Address.getList({pageNumber: 1, pageSize: 100})
        })
    }

    addList(itm, ind) {
        return <Swipeout key={`lists_${ind}`} autoClose left={[{
                text: '选择',
                onPress: () => this.toXplan(itm),
                style: {
                    backgroundColor: 'orange',
                    color: 'white'
                }
            }
        ]} right={[
            {
                text: '编辑',
                onPress: () => location.hash = `#/operate?id=${itm.id}`,
                style: {
                    backgroundColor: 'red',
                    color: 'white'
                }
            }, {
                text: '删除',
                onPress: () => {
                    this.openDeleteModal();
                    this.setState({addressId: itm.id, defaultAddress: itm.defaultAddress});
                },
                style: {
                    backgroundColor: '#ccc',
                    color: 'white'
                }
            }
        ]}>
            <dl id={itm.id} key={`lists_${ind}`} onClick={this.toXplan.bind(this, itm)}>
                <dd>
                    {itm.name}
                </dd>
                <dd className='tel'>
                    {itm.phone}
                </dd>
                <dt>
                    <i style={{
                        display: (itm.defaultAddress
                            ? ''
                            : 'none')
                    }}>【默认】</i>
                    {itm.province}{itm.city}{itm.county}{itm.address}
                </dt>
            </dl>
        </Swipeout>
    }

    render() {
        const [lists,
            list] = [
            [], this.props.address.list || []
        ];
        list.forEach((itm, ind) => lists.push(this.addList(itm, ind)));
        if (!this.props.showLoading && !list.length) {
            lists.push(
                <p key='none' id='none'>您暂时还没填写过收货地址</p>
            );
        }

        return <section id='list' className='select_address' style={{
            minHeight: this.props.minHeight
        }}>
            {lists}
            <a id='from_show' href="javascript:;" onClick={() => location.hash = '#/change'}>管理地址</a>
            <AddMessageModal isOpen={this.state.addMessageModalIsOpen} onAfterOpen={this.openAddMessageModal.bind(this)} onRequestClose={this.closeAddMessageModal.bind(this)} closeTimeoutMS={150}>
                <section className='delete'>
                    <p>收货地址信息不完整，请点击补充</p>
                    <dl>
                        <dd onClick={this.closeAddMessageModal.bind(this)}>关闭</dd>
                        <dd className='red' onClick={() => {
                            location.hash = `operate?id=${this.state.placeId}`
                        }}>补充信息</dd>
                    </dl>
                </section>
            </AddMessageModal>
            <DeleteModal isOpen={this.state.deleteModalIsOpen} onAfterOpen={this.openDeleteModal.bind(this)} onRequestClose={this.closeDeleteModal.bind(this)} closeTimeoutMS={150}>
                <section className='delete'>
                    <p className='center'>确认要删除吗？</p>
                    <dl>
                        <dd onClick={this.closeDeleteModal.bind(this)}>取消</dd>
                        <dd onClick={this.removeNow.bind(this)}>确定</dd>
                    </dl>
                </section>
            </DeleteModal>
        </section>
    }
}

function select(state) {
    const list = state.setAddress.ADDRESS_GETLIST;
    return {
        address: list && list.pager || {},
        minHeight: state.minHeight
    }
}

export default connect(select)(Message(SelectAddress))

// module.export = connect(select)(Message(SelectAddress))
