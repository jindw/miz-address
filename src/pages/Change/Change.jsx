import React, {Component} from 'react'
import {Message} from "./Message.jsx"
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'
import './Change.scss';
import {connect} from 'react-redux'
import * as actions from '../../app/actions'

class List extends Component {

    constructor(props) {
        super(props);
        this.state = {
            minHeight: 0,
            errTop: $(window).height() * 0.3,
            errClass: 'none'
        }
        this.hideErr = null;
    }

    componentDidMount() {
        this.props.dispatch(actions.setTitle({title: '地址管理', backBtn: true}));
    }

    componentWillUnmount() {
        clearTimeout(this.hideErr);
    }

    addList(itm, ind) {
        return <dl key={`lists_${ind}`} onClick={() => location.hash = `#/operate?id=${itm.id}`}>
            <dd>
                {itm.name}
            </dd>
            <dd className='tel'>
                {itm.phone}
            </dd>
            <dt>
                <i style={{
                    display: (itm.defaultAddress
                        ? ''
                        : 'none')
                }}>【默认】</i>
                {itm.province}{itm.city}{itm.county}{itm.address}
            </dt>
            <Arrow fill='#c7c7cc'/>
        </dl>
    }

    render() {
        const lists = [];
        const {address, minHeight} = this.props;
        const list = address.list || [];

        list.forEach((itm, ind) => lists.push(this.addList(itm, ind)));

        if (!this.props.showLoading && !list.length) {
            lists.push(
                <p key='none' id='none'>您暂时还没填写过收货地址</p>
            );
        }

        return <section className='select_address rightSvg' id='list' style={{
            minHeight
        }}>
            {lists}
            <a href="javascript:;" onClick={() => {
                if (list.length >= 5) {
                    if (this.state.errClass !== 'none')
                        return;
                    this.setState({err: '最多只能添加5个地址哦', errClass: 'zoomIn'});
                    this.hideErr = setTimeout(() => {
                        this.setState({errClass: 'zoomOut'});
                        this.hideErr = setTimeout(() => {
                            this.setState({errClass: 'none'});
                        }, 800);
                    }, 2000);
                } else {
                    location.hash = '#/operate';
                }
            }}>新增地址</a>
            <section id='err' style={{
                top: this.state.errTop
            }} className={`animated ${this.state.errClass}`}>
                <a href="javascript:;">{this.state.err}</a>
            </section>
        </section>
    }
}

function select(state) {
    const list = state.setAddress.ADDRESS_GETLIST;
    return {
        address: list && list.pager || {},
        minHeight: state.minHeight
    }
}

export default connect(select)(Message(List))
