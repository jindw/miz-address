import Arrow from './Svg_Arrow_Left.jsx'
import React,{Component} from 'react'
import _assign from 'lodash/assign'
import { Link } from 'react-router'
import './Header.scss'
// require.ensure([],require=>require('./Header.scss'));
import { connect } from 'react-redux'

class Header extends Component {

    constructor(props) {
        super(props);
    }

    back(){
        if(window.history.length <= 1){
            location.hash = '/#';
            return;
        }
        if(typeof(this.props.backBtn) === 'string'){
            if(this.props.backBtn === 'mzlicai://close'){//安卓的需求
                location.href = 'mzlicai://close'
            }else{
                location.hash = this.state.backBtn;
            }
        }else if(history.length < 2){
            location.hash = '/';
        }else{
            history.back(-1);
        }
    }

    render() {
        const myTitle = this.props.myTitle;
        const rightBtn = myTitle.rightBtn;
        const back_dom = myTitle.backBtn ? <div onClick={this.back.bind(this)}>
                        <Arrow fill='#c7c7cc'/>
                    </div> : null;

        let right_btn;
        if(rightBtn){
            right_btn = <Link className='top_right' to={rightBtn[1]}>{rightBtn[0]}</Link>
        }

        return <header ref='header' style={{display:(this.props.show?'none':''),height:(this.props.show?'0':'')}}>
                <div className="left">
                    {back_dom}
                </div>
                <div id="js_header_title" className="title">{myTitle.title}</div>
                <div>
                    {right_btn}
                </div>
            </header>
    }
};

function select(state) {
    return {
        myTitle:state.setTitle,
    }
}

export default connect(select)(Header);
