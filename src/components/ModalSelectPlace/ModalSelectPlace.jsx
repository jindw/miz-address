import React,{Component} from  'react'
import Modal from 'react-modal'
import Arrow from '../Header/Svg_Arrow_Left.jsx'
import './ModalSelectPlace.scss'
import { connect } from 'react-redux'
import * as actions from '../../app/actions'
import {myCityList} from './citylist'

class ModalSelectPlace extends Component {

    constructor(props) {
        super(props);
        this.state = {
        	cityList:{
                sub:[],
            },
            countyList:[],
            minHeight:0,
            openTime:0,
        }
        this.countyList = {};
    }

    componentDidMount() {
		this.setState({
			minHeight: $(window).height() - $('header').height(),
		});
	}

    openModalPlace(){
        this.props.dispatch(actions.setTitle({
            title:'选择地区',
            backBtn:false,
        }));
        this.sw =  new Swiper('.swiper-container',{
            swipeHandler : 'none',
            effect : 'coverflow',
            slidesPerView: 1,
            centeredSlides: true,
            coverflow: {
                rotate: 30,
                stretch: 10,
                depth: 60,
                modifier: 2,
                slideShadows : true
            },
            speed:200,
        });

        let moveTop;
        $('.bankList').on('touchstart',e=>{
            moveTop = e.touches[0].pageY + $('.bankList').scrollTop();
        }).on('touchmove',e=>{
            e.preventDefault();
            let moveNow = e.touches[0].pageY;
            $('.bankList').scrollTop(moveTop - moveNow);
        });

        this.refs.cityList.style.height = `${this.state.minHeight}px`;
        this.noInput = setInterval(()=>{
            $('input,textarea').blur();
        },1000)
    }

    closeModalPlace(){
        this.props.dispatch(actions.setTitle({
            title:'编辑地址',
            backBtn:true,
        }));
        this.props.closePlace();
        clearInterval(this.noInput);
    }

    SelectCity(ind){
        this.setState({cityList: myCityList[ind]});
        this.sw.slideNext();
    }

    selectCounty(ind){
        this.countyList = this.state.cityList.sub[ind];
        if(this.countyList.sub){
            this.setState({countyList:this.countyList.sub});
            this.sw.slideNext();
        }else{
            this.selectPlace(false);
        }
        
    }

    selectPlace(e){
        this.closeModalPlace();
        this.props.success(this.state.cityList.name,this.countyList.name,e.name);
    }

    prevModalPlace(){
        this.sw.slidePrev();
        $('.bankList').scrollTop(0);
    }

    render() {
    	const [provinces,cities,county] = [[],[],[]];

    	myCityList.forEach((itm,ind)=>provinces.push(<dd key={`province_${ind}`} 
            onClick={this.SelectCity.bind(this,ind)}>{itm.name}<Arrow fill='#3366cc'/></dd>));

        this.state.cityList.sub.forEach((itm,ind)=>cities.push(<dd key={`city_${ind}`} 
            onClick={this.selectCounty.bind(this,ind)}>{itm.name}<Arrow fill='#3366cc'/></dd>));

        this.state.countyList.forEach((itm,ind)=>county.push(<dd key={`county_${ind}`} 
            onClick={this.selectPlace.bind(this,itm)}>{itm.name}<Arrow fill='#3366cc'/></dd>));

        return <Modal
                isOpen={this.props.open}
                onAfterOpen={this.openModalPlace.bind(this)}
                onRequestClose={this.closeModalPlace.bind(this)}
                closeTimeoutMS={150}
                style={{
                    overlay : {
                        top:$('header').height()
                    },
                    content : {
                        width:'100%',
                        height:'100%',
                        backgroundColor:'#eee',
                        left:0,
                    }
                }}
            >   
                <div ref='cityList' className="swiper-container selectCity">
                    <div ref='swiperWrapper' className="swiper-wrapper rightSvg">
                        <div className="swiper-slide">
                            <p className='cityTitle citylist' onClick={this.closeModalPlace.bind(this)}><i></i><label>请选择地区</label><Arrow fill='#3366cc'/></p>
                            <dl className='bankList citylist'>
                                {provinces}
                                <dd></dd>
                            </dl>
                        </div>
                        <div className="swiper-slide">
                            <p className='cityTitle citylist' onClick={this.prevModalPlace.bind(this)}><i></i><label>请选择地区</label><Arrow fill='#3366cc'/>
                                <span>{this.state.cityList.name}</span>
                            </p>
                            <dl style={{height:this.state.cityHeight}} className='rightSvg bankList citylist'>
                                {cities}
                                <dd></dd>
                            </dl>
                        </div>
                        <div className="swiper-slide">
                            <p className='cityTitle citylist' onClick={this.prevModalPlace.bind(this)}><i></i><label>请选择地区</label><Arrow fill='#3366cc'/>
                                <span>{this.state.cityList.name} {this.countyList.name}</span>
                            </p>
                            <dl className='rightSvg bankList citylist'>
                                {county}
                                <dd></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </Modal>
    }
};

export default connect()(ModalSelectPlace);