var webpack = require('webpack')
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
      apps: './src/app/app.jsx',
      common:[
        'react',
        'jquery',
        'swiper',
        './node_modules/swiper/dist/css/swiper.css',
        'animate.css',
        'es6-promise',
        'redux',
        'fastclick',
        'react-dom',
        'react-modal',
        'react-redux',
        'react-router',
        'rc-swipeout'
      ]
  },

  output: {
      path: path.resolve(__dirname, './dist'),
      filename: '[name].min.js',
      publicPath: 'https://cdn.mizlicai.com/miz-address/'
  },

    // output: {
    //     // path: './dist',
    //     path: path.resolve(__dirname, './dist'),
    //     filename: 'app.min.js',
    //     publicPath: './',
    // },

    devServer: {
        historyApiFallback: true,
        noInfo: true,
        inline: true,
    },

    module: {
        rules: [{
            test: /\.(jsx|js)?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',

            query: {
                presets: ['es2015', 'react']
            }
        }, {
            test: /\.(css|scss)$/,
            loader: "style-loader!css-loader!postcss-loader!sass-loader"
        }]
    },

    // devtool: 'hidden-source-map',
    devtool:'nosources-source-map',

    // performance: {
    //     hints: false
    // },
    plugins: [
        // 开发环境配置
        new webpack.DefinePlugin({
            __LOCAL__: false, // 测试环境
            __PRO__: true, // 生产环境
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: true,
            compress: {
                warnings: false
            }
        }),
        new webpack.ProvidePlugin({
          '$':'jquery',
          Swiper: "swiper",
        }),
        new HtmlWebpackPlugin({
            template: './index.html',
        }),
        new webpack.optimize.CommonsChunkPlugin({names: ['common'], minChunks: Infinity})
    ]

}
