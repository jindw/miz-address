>miz理财-地址管理

`browserSync.stream()`:css加载不刷新页面


>已启用webpack2，废弃gulp



>yarn 替换 npm

## yarn 安装
- mac:`brew install yarn`
- win:https://yarnpkg.com/latest.msi
- linux(Ubuntu):
```curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn
 ```

 >初始化:yarn init
 >生成**不可修改**的yarn.lock和package.json文件

 ## 添加依赖

 ```
 yarn add react     //安装react并在package.json的dependencies添加
 yarn add react@15.4.2 //安装指定版本
 yarn add react -D   //devDependencies下添加

 ```
 >不知道版本号,可以和npm一样,写一个保证不存在的版本号,如:
 现在并没有 `react-router-dom` 的5以上的版本

 我们输入`yarn add react-router-dom@5`,会输出:
 ```
 yarn add v0.19.1
[1/4] 🔍  Resolving packages...
Couldn't find any versions for "react-router-dom" that matches "5"
? Please choose a version from this list: (Use arrow keys)
❯ 4.0.0-beta.6
  4.0.0-beta.5
  4.0.0-beta.4
  4.0.0-beta.3
  4.0.0-beta.2
  4.0.0-beta.1
  0.0.0
 ```
 >上下移动光标选择,再回车,即可安装选择的版本,非常方便
 但也有一点不足,这里支持选择版本,但`package.json`里的版本号确实最先写的5,希望以后能修复

 ## 升级(upgrade)
 >只需要把`add` 改成 `upgrade`即可
  ```
 yarn upgrade react     
 yarn upgrade react@15.4.1
 yarn upgrade react -D  //不会改变位置
 ```

 ## 删除依赖
 >yarn remove react

 ## 安装所有的依赖
 >类似npm i ---- `brew` 或 `brew install`

 ## npm script
 >代码不用做任何修改,执行 `yarn start` ,`yarn run dev` 等等即可
