var webpack = require('webpack')
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        apps: './src/app/app.jsx',
        common: [
            'react',
            'jquery',
            'swiper',
            './node_modules/swiper/dist/css/swiper.css',
            'animate.css',
            'es6-promise',
            'redux',
            'fastclick',
            'react-dom',
            'react-modal',
            'react-redux',
            'react-router',
            'rc-swipeout'
        ]
    },

    output: {
        path: path.resolve(__dirname, './dist'),
        filename: '[name].js' //最终打包生产的文件名
        // publicPath: '/dist/'
    },

    devServer: {
        historyApiFallback: true,
        noInfo: true,
        inline: true
    },

    module: {
        rules: [
            {
                test: /\.jsx$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react']
                }
            }, {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            }, {
                test: /\.json$/,
                loader: 'json-loader',
                exclude: /node_modules/
            }, {
                test: /\.(css|scss|less)$/,
                loader: "style-loader!css-loader!postcss-loader!sass-loader"
            }
        ]
    },

    // devtool: 'hidden-source-map',
    devtool: 'eval',
    watch: true,
    performance: {
        hints: false
    },
    plugins: [
        // 开发环境配置
        new webpack.DefinePlugin({
            __LOCAL__: true, // 测试环境
            __PRO__: false, // 生产环境
            // 'process.env': {
            //     NODE_ENV: '"production"'
            // }
        }),
        new HtmlWebpackPlugin({template: './index.html'}),
        new webpack.ProvidePlugin({'$': 'jquery', Swiper: "swiper"}),
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: true,
            compress: {
                warnings: false
            }
        }),
        new webpack.optimize.CommonsChunkPlugin({names: ['common'], minChunks: Infinity})
    ]

}
